<div class="featured one-third">
    <header class="s-title">
        <h2 class="ribbon star">Featured member</h2>
    </header>
    <?php 
    $sql="SELECT * FROM `chef_detail` WHERE is_active='1' ORDER BY id DESC LIMIT 1";
    foreach ($db->query($sql)as $row){
        $chef=$row;
        
  
    }
    ?>
    <article class="entry">
        <figure>
            <img src="<?= SITE_IMG_PATH ?><?php echo $chef['image'];?>" style="height: 520px !important; width:372px;" alt="chef_image" />
            <figcaption><a href="#"><i class="icon icon-themeenergy_eye2"></i> <span>View member</span></a></figcaption>
        </figure>
        <div class="container">
            <h2><a href="#"><?php echo $chef['name'];?></a></h2>
            <blockquote><i class="fa fa-quote-left"></i><?php echo $chef['descriptions'];?></blockquote>
            
            <div class="actions">
                <div>
                    <a href="#" class="button" alt="under_constructions">Check out her recipes</a>
<!--                    <div class="more"><a href="#">See past featured members</a></div>-->
                </div>
            </div>
         
        </div>
       
    </article>
</div>