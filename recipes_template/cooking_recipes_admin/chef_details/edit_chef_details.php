<!--/* Author : Munira *-->
<?php
session_start();
include_once '../lib/settings.php';
?>
<?php include_once '../lib/connection.php'; ?>


<?php include_once '../element/headPart.php'; ?>
<title>Add Chef | Dashboard</title>

<body class="skin-black">
    <!-- header logo: style can be found in header.less -->
<?php include_once '../element/navbar.php'; ?>
    <!-- Header Navbar: style can be found in header.less -->


    <div class="wrapper row-offcanvas row-offcanvas-left">
        <!-- Left side column. contains the logo and sidebar -->

        <!-- Sidebar user panel -->

<?php include_once '../element/sidebar.php'; ?>

        <!-- /.sidebar -->




        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Update chef Details
                    <small>Control panel</small>
                </h1>
                <!--                    <ol class="breadcrumb">
                                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                                        <li class="active">Dashboard</li>
                                    </ol>-->
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Small boxes (Stat box) -->
                <div class="row">

                    <div class="col-lg-6 col-lg-offset-3">
                        <div class="panel panel-default">
                            <div class="panel-heading text-center">
                                Update chef Details
                            </div>
                            <div class="panel-body">
                              
                                <div class="row">

                                    <?php
                                    //build query

                                    $queryup = "SELECT * FROM chef_detail WHERE id = " . $_GET['id'];
                                    //excute the query useing php
                                    foreach ($db->query($queryup) as $row) {
                                        $editDatapart = $row;
                                    }
                                    ?>
                                    <div class="col-lg-12">
                                        <form  action="<?= APP_PATH ?>chef_details/update_chef_details.php" method="post" enctype="multipart/form-data">
                                            <input type="hidden" id="id" name="id" class="form-control" value="<?= $editDatapart['id']; ?>">
                                            <div class="form-group">
                                                <label for="chef_name">Chef Name</label>
                                                <input class="form-control" type="text" name="chef_name" id="chef_name" value="<?= $editDatapart['name']; ?>" required="required" autofocus="autofocus" alt="chef_name" placeholder="Enter Chef Name Here">
                                                <p class="help-block"></p>
                                            </div>


                                            <div class="form-group">
                                                <label for="chef_descriptions">Descriptions </label>
                                                <textarea class="form-control"  name="chef_descriptions" id="chef_descriptions"  required="required" alt="chef_descriptions" placeholder="Enter Chef Descriptions  Here"><?= $editDatapart['descriptions']; ?></textarea>

                                            </div>





                                            <div class="form-group">
                                                <label for="recipes_date">Recipes Date</label>
                                                <input class="form-control" type="text" name="recipes_date" id="date_picker" required="required" value="<?= $editDatapart['date']; ?>"  alt="recipes_date" placeholder="Enter Recipes submit Date Here    YYYY-MM-DD">

                                            </div>
                                            <div class="form-group">
                                                <label for="chef_image">Recipes Image</label><br>
                                               
                                                
                                                <img src="<?= APP_PATH ?>images/<?php echo $editDatapart['image']; ?>"  alt="chef_image"   style="width:304px;height:228px; border:8px inset black;" class="img-thumbnail img-responsive"><br><br>

                                                <input  type="file" name="chef_image" id="chef_image"  alt="chef_image" aria-describedby="fileHelp" ><br>
                                                <input type="hidden" value="<?php echo $editDatapart['image']; ?>" name="ex_chef_image" />

                                            </div>

                                            <div class="form-group">
                                                <label>Chef status  is active/inactive now</label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="status" id="status_active" <?php if ($editDatapart['is_active']==1){ ?>checked="checked"<?php } ?> > Yes
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="status" id="status_inactive" <?php if ($editDatapart['is_active']==0){ ?>checked="checked"<?php } ?>> No
                                                </label>
                                            </div>
                                            <button type="submit" class="btn btn-default">Update Chef</button>
<!--                                            <button type="reset" class="btn btn-default">Clear</button>-->
                                        </form>
                                    </div>
                                </div>
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>


                </div><!-- /.row (main row) -->

            </section><!-- /.content -->

        </aside><!-- /.right-side -->
    </div><!-- ./wrapper -->




    <!--footer part start here-->
<?php include_once '../element/footer.php'; ?>    
    <!--footer part end here-->
    <!--    datepicker start here-->




























