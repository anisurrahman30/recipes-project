<!--/* Author : Munira *-->
<?php include_once '../lib/settings.php'; ?>
<?php include_once '../lib/connection.php'; ?>


<?php include_once '../element/headPart.php'; ?>
<title>Add Recipes | Dashboard</title>

<body class="skin-black">
    <!-- header logo: style can be found in header.less -->
    <?php include_once '../element/navbar.php'; ?>
    <!-- Header Navbar: style can be found in header.less -->


    <div class="wrapper row-offcanvas row-offcanvas-left">
        <!-- Left side column. contains the logo and sidebar -->

        <!-- Sidebar user panel -->

        <?php include_once '../element/sidebar.php'; ?>

        <!-- /.sidebar -->




        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                  Recipes Category Edit
                    <small>Control panel</small>
                </h1>
                <!--                    <ol class="breadcrumb">
                                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                                        <li class="active">Dashboard</li>
                                    </ol>-->
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Small boxes (Stat box) -->
                <div class="row">

                    <div class="col-lg-6 col-lg-offset-3">
                        <div class="panel panel-default">
                            <div class="panel-heading text-center">
                               Update Recipes Category
                            </div>
                               <?php
                          

                            //build query

                            $queryup = "SELECT * FROM recipes_category WHERE id = " . $_GET['id'];
                            //excute the query useing php
                            foreach ($db->query($queryup) as $row) {
                                $editDatapart = $row;
                            }
                            ?>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <form  action="<?= APP_PATH ?>recipes_category/action_recipes_category.php" method="post">
                                            <div class="form-group">
                                                <label for="recipes_category">Recipes Category Name</label>
                                                <input class="form-control" type="text" name="recipes_category" id="recipes_category" value="<?= $editDatapart['name'];?>" required="required" autofocus="autofocus" alt="recipes_category" placeholder="Enter Recipes Category Name Here">
                                                <p class="help-block"></p>
                                            </div>

               





                                            <div class="form-group">
                                                <label for="recipes_category_date">Recipes Date</label>
                                                <input class="form-control" type="date" name="recipes_category_date" id="date_picker" value="<?= $editDatapart['date'];?>" required="required" alt="recipes_category_date" placeholder="Enter Recipes submit Date Here    YYYY-MM-DD">

                                            </div>
                                           


                                            <div class="form-group">
                                                <label>Recipes category is menu's add now</label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="category" id="category_featured_active" <?php if($editDatapart['is_top_nav']==1){?>checked="checked"<?php } ?>> Yes
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="category" id="category_featured_active" <?php if($editDatapart['is_top_nav']==0){?>checked="checked"<?php } ?>> No
                                                </label>
                                            </div>
                                          
                                            
                                            <div class="form-group">
                                                <label>Recipes category status  is active/inactive now</label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="category_status" id="category_status_active" <?php if($editDatapart['status']==1){?>checked="checked" <?php } ?>> Yes
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="category_status" id="category_status_active" <?php if($editDatapart['status']==0){?>checked="checked"<?php } ?>> No
                                                </label>
                                            </div>
                                            <button type="submit" class="btn btn-default">Update Recipes Category</button>
                                            <button type="reset" class="btn btn-default">Clear</button>
                                        </form>
                                    </div>
                                </div>
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>


                </div><!-- /.row (main row) -->

            </section><!-- /.content -->

        </aside><!-- /.right-side -->
    </div><!-- ./wrapper -->




    <!--footer part start here-->
    <?php include_once '../element/footer.php'; ?>    
    <!--footer part end here-->
<!--    datepicker start here-->
 <script>
            $(document).ready(function () {
                $("#recipes_date").datepicker();

            });
        </script> 


























