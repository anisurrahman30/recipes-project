<!--/* Author : Munira *-->
<?php
session_start();
include_once '../lib/settings.php';
?>
<?php include_once '../lib/connection.php'; ?>


<?php include_once '../element/headPart.php'; ?>
<title>Update Admin | Dashboard</title>

<body class="skin-black">
    <!-- header logo: style can be found in header.less -->
    <?php include_once '../element/navbar.php'; ?>
    <!-- Header Navbar: style can be found in header.less -->


    <div class="wrapper row-offcanvas row-offcanvas-left">
        <!-- Left side column. contains the logo and sidebar -->

        <!-- Sidebar user panel -->

        <?php include_once '../element/sidebar.php'; ?>

        <!-- /.sidebar -->




        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Dashboard
                    <small>Control panel</small>
                </h1>
                <!--                    <ol class="breadcrumb">
                                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                                        <li class="active">Dashboard</li>
                                    </ol>-->
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Small boxes (Stat box) -->
                <div class="row">

                    <div class="col-lg-6 col-lg-offset-3">
                        <div class="panel panel-default">
                            <div class="panel-heading text-center">
                                Updated Admin
                            </div>

                            <?php
                            //build query

                            $queryup = "SELECT * FROM customer WHERE id = " . $_GET['id'];
                            //excute the query useing php
                            foreach ($db->query($queryup) as $row) {
                                $editDatapart = $row;
                            }
                            ?>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <form  action="<?= APP_PATH ?>admin/update_admin.php" method="post" enctype="multipart/form-data">
                                            <input type="hidden" id="id" name="id" class="form-control" value="<?= $editDatapart['id']; ?>">
                                            <div class="form-group">
                                                <label for="recipesName">Admin Name</label>
                                                <input class="form-control" value="<?= $editDatapart['name']; ?>" type="text" name="name" id="name" required="required" autofocus="autofocus" placeholder="Enter Recipes Name Here">
                                                <p class="help-block"></p>
                                            </div>

                                            <div class="form-group">
                                                <label for="recipes_ingredients">Admin Email  </label>

                                                <input class="form-control"  value="<?= $editDatapart['email']; ?>"  type="text"  name="email" id="email" required="required" placeholder="Enter Email" />
                                            </div>


                                            <div class="form-group">
                                                <label for="recipes_directions"> <input 
                                                        <?php 
                                                        if($editDatapart['status']=="active")
                                                        {
                                                            ?>
                                                            checked="checked"
                                                            <?php
                                                        }    
                                                            ?>
                                                        class="form-control" value="1" name="status" type="checkbox" /> Is Active </label>
                                            </div>
                                            <button type="submit" class="btn btn-default">Update Admin</button>
                                            <!--                                            <button type="reset" class="btn btn-default">Clear</button>-->
                                        </form>
                                    </div>
                                </div>
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>


                </div><!-- /.row (main row) -->

            </section><!-- /.content -->

        </aside><!-- /.right-side -->
    </div><!-- ./wrapper -->




    <!--footer part start here-->
    <?php include_once '../element/footer.php'; ?>    
    <!--footer part end here-->
    <!--    datepicker start here-->
    <script>
        $(document).ready(function () {
            $("#recipes_date").datepicker();

        });
    </script> 



























