<!--/* Author : Munira *-->

<?php
session_start();
include_once '../lib/settings.php';
?>
<?php include_once '../lib/connection.php'; ?>


<?php include_once '../element/headPart.php'; ?>
<title>Add Recipes video link| Dashboard</title>

<body class="skin-black">
    <!-- header logo: style can be found in header.less -->
    <?php include_once '../element/navbar.php'; ?>
    <!-- Header Navbar: style can be found in header.less -->


    <div class="wrapper row-offcanvas row-offcanvas-left">
        <!-- Left side column. contains the logo and sidebar -->

        <!-- Sidebar user panel -->

        <?php include_once '../element/sidebar.php'; ?>

        <!-- /.sidebar -->




        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Add Recipes video link
                    <small>Control panel</small>
                </h1>
                <!--                    <ol class="breadcrumb">
                                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                                        <li class="active">Dashboard</li>
                                    </ol>-->
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Small boxes (Stat box) -->
                <div class="row">

                    <div class="col-lg-6 col-lg-offset-3">
                        <div class="panel panel-default">
                            <div class="panel-heading text-center">
                                Add Recipes video link
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <form  action="<?= APP_PATH ?>recipes_video/action_recipes_video_link.php" method="post">

                                            <?php $sql = "SELECT * FROM recipes ORDER BY id DESC"; ?>

                                            <div class="form-group">
                                                <label for="recipes_name">Recipes name</label>

                                                <select  class="form-control" type="text" name="recipes_name" id="recipes_name_video" required="required" alt="recipes_name">
                                                    <option>Select Recipes</option>

                                                    <?php foreach ($db->query($sql) as $row) { ?>

                                                        <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                                                    <?php } ?>
                                                </select>


                                            </div>

                                            <div class="form-group">
                                                <label for="recipes_video">Recipes Video Link</label>
                                                <input class="form-control" type="text" name="recipes_video_link" id="recipes_video_link" required="required" autofocus="autofocus" alt="recipes_video" placeholder="Enter Recipes Video Link Here">
                                                <p class="help-block"></p>
                                            </div>

                                            <div class="form-group">
                                                <label for="recipes_video_date">Recipes Video Date</label>
                                                <input class="form-control" type="date" name="recipes_video_date" id="date_picker" required="required" alt="recipes_video_date" placeholder="Enter Recipes Video submit Date Here    YYYY-MM-DD">

                                            </div>



                                            <div class="form-group">
                                                <label>Recipes Video status  is active/inactive now</label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="recipes_video" id="recipes_video_status_active" value="1"> Yes
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="recipes_video" id="recipes_video_status_active" value="0"> No
                                                </label>
                                            </div>
                                            <button type="submit" class="btn btn-default">Add Recipes video</button>
                                            <button type="reset" class="btn btn-default">Clear</button>
                                        </form>
                                    </div>
                                </div>
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>


                </div><!-- /.row (main row) -->

            </section><!-- /.content -->

        </aside><!-- /.right-side -->
    </div><!-- ./wrapper -->




    <!--footer part start here-->
    <?php include_once '../element/footer.php'; ?>    
    <!--footer part end here-->
    <!--    datepicker start here-->
    <script>
        $(document).ready(function () {
            $("#recipes_date").datepicker();

        });
    </script> 


























