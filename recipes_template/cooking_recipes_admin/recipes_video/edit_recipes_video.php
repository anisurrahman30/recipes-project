<!--/* Author : Munira *-->

<?php
//session_start();
include_once '../lib/settings.php';
?>
<?php include_once '../lib/connection.php'; ?>


<?php include_once '../element/headPart.php'; ?>
<title>Update Recipes video link| Dashboard</title>

<body class="skin-black">
    <!-- header logo: style can be found in header.less -->
    <?php include_once '../element/navbar.php'; ?>
    <!-- Header Navbar: style can be found in header.less -->


    <div class="wrapper row-offcanvas row-offcanvas-left">
        <!-- Left side column. contains the logo and sidebar -->

        <!-- Sidebar user panel -->

        <?php include_once '../element/sidebar.php'; ?>

        <!-- /.sidebar -->




        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Update Recipes video link
                    <small>Control panel</small>
                </h1>
                <!--                    <ol class="breadcrumb">
                                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                                        <li class="active">Dashboard</li>
                                    </ol>-->
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Small boxes (Stat box) -->
                <div class="row">

                    <div class="col-lg-6 col-lg-offset-3">
                        <div class="panel panel-default">
                            <div class="panel-heading text-center">
                                Update Recipes video link
                            </div>
                            <?php
                            $sql = "SELECT * FROM recipes_video WHERE id='" . $_GET['id'] . "'";
                            foreach ($db->query($sql) as $row) {
                                $editDatapart = $row;
                            }
                            ?>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <form  action="<?= APP_PATH ?>recipes_video/update_recipes_video_link.php" method="post" enctype="multipart/form-data">
                                            <input type="hidden" id="id" name="id" class="form-control" value="<?php echo $editDatapart['id']; ?>">
                                            <div class="form-group">

                                                <label for="recipes_name">Recipes name</label>

                                                <select  class="form-control" type="text" name="recipes_name" id="recipes_name_video" required="required" alt="recipes_name">
                                                    <option>Select Recipes</option>
                                                    <?php
                                                    $sql1 = "SELECT * FROM recipes";
                                                    foreach ($db->query($sql1) as $row1) {
                                                        ?>
                                                        <option 
                                                        <?php
                                                        if ($row1['id'] == $editDatapart['recipes_id']) {
                                                            ?>
                                                                selected="selected"  
                                                                <?php
                                                            }
                                                            ?>
                                                            value="<?php echo $row1['id']; ?>"><?php echo $row1['name']; ?></option>
                                                            <?php
                                                        }
                                                        ?>


                                                </select>


                                            </div>

                                            <div class="form-group">
                                                <label for="recipes_video">Recipes Video Link</label>
                                                <input class="form-control" type="text" name="recipes_video_link" id="recipes_video_link" value="<?php echo $editDatapart['link']; ?>"   alt="recipes_video" placeholder="Enter Recipes Video Link Here">
                                                <p class="help-block"></p>
                                            </div>

                                            <div class="form-group">
                                                <label for="recipes_video_date">Recipes Video Date</label>
                                                <input class="form-control" type="date" name="recipes_video_date" id="date_picker"  value="<?php echo $editDatapart['date']; ?>" alt="recipes_video_date" placeholder="Enter Recipes Video submit Date Here    YYYY-MM-DD">

                                            </div>



                                            <div class="form-group">
                                                <label>Recipes Video status  is active/inactive now</label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="recipes_video" id="recipes_video_status_active" <?php if ($editDatapart['is_active'] == 1) { ?> checked="checked" <?php } ?>> Yes
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="recipes_video" id="recipes_video_status_active" <?php if ($editDatapart['is_active'] == 0) { ?> checked="checked" <?php } ?>> No
                                                </label>
                                            </div>
                                            <button type="submit" class="btn btn-default">Update Recipes video</button>
<!--                                            <button type="reset" class="btn btn-default">Clear</button>-->
                                        </form>
                                    </div>
                                </div>
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>


                </div><!-- /.row (main row) -->

            </section><!-- /.content -->

        </aside><!-- /.right-side -->
    </div><!-- ./wrapper -->




    <!--footer part start here-->
    <?php include_once '../element/footer.php'; ?>    
    <!--footer part end here-->
    <!--    datepicker start here-->
    <script>
        $(document).ready(function () {
            $("#recipes_date").datepicker();

        });
    </script> 


























