

<header class="header">
    <a href="<?=APP_PATH?>index.php" class="logo">
        <!-- Add the class icon to your logo image or logo icon to add the margining -->
        Admin Panel
    </a>
</header>
<nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </a>
    <div class="navbar-right">
        <ul class="nav navbar-nav">
            <!-- Messages: style can be found in dropdown.less-->

            <!-- Notifications: style can be found in dropdown.less -->

            <!-- Tasks: style can be found in dropdown.less -->

            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i>
                    <span><?=$_SESSION['SESS_ADMIN_NAME']?> <i class="caret"></i></span>
                </a>
                <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header bg-light-blue">
                       <p>
                            <?=$_SESSION['SESS_ADMIN_NAME']?>
                            <small>Member since Nov. 2012</small>
                        </p>
                    </li>

                    <!-- Menu Footer-->

                    <!--sign out and profile start here-->
                    <li class="user-footer">
                        <div class="pull-left">
                            <a href="#" class="btn btn-default btn-flat">Profile</a>
                        </div>
                        <div class="pull-right">
                            <a href="<?= APP_PATH ?>logout.php" class="btn btn-default btn-flat">Sign out</a>
                        </div>
                    </li>
                    <!--sign out and profile end here-->
                </ul>
            </li>
        </ul>
    </div>
</nav>