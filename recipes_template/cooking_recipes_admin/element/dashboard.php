
<?php
$sql_total="SELECT

(SELECT COUNT(`id`) as total  FROM recipes)as total_recipes,
(SELECT COUNT(`id`) as total  FROM recipes_category)as total_recipes_category,
(SELECT COUNT(`id`) as total  FROM customer)as total_user,
(SELECT COUNT(`id`) as total  FROM chef_detail)as total_chef";
$res_total = $db->query($sql_total);
foreach($res_total as $row):


?>

<section class="content">

    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>
                     <?php echo $row['total_recipes'];?>
                    </h3>
                    <p>
                        Total Recipes
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
                <a href="#" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>
                      <?php echo $row['total_recipes_category'];?>
                    </h3>
                    <p>
                        Recipes Category
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>
                       <?php echo $row['total_user'];?>
                    </h3>
                    <p>
                        User Registrations
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="#" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>
                       <?php echo $row['total_chef'];?>
                    </h3>
                    <p>
                        Chefs
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-pie-graph"></i>
                </div>
                <a href="#" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
    </div><!-- /.row -->

    <!-- top row -->
    <div class="row">
        <div class="col-xs-12 connectedSortable">
            aaaaaa
        </div><!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Main row -->
    <div class="row">

        <!-- right col (We are only adding the ID to make the widgets sortable)-->

    </div><!-- /.row (main row) -->
    sss
</section><!-- /.content -->
<?php endforeach; ?>