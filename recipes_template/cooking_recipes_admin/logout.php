<?php

session_start();


//bulid query
include_once 'lib/settings.php';
include_once 'lib/connection.php';



unset($_SESSION['SESS_ADMIN_ID']);
unset($_SESSION['SESS_ADMIN_NAME']);

session_regenerate_id();
$_SESSION['SESS_MSG_ERROR'] = 'Failed Admin Login.';
session_write_close();
header('location: login.php');
exit();
?>