<!--/* Author : Munira *-->
<?php
session_start();
include_once '../lib/settings.php';
?>
<?php include_once '../lib/connection.php'; ?>


<?php include_once '../element/headPart.php'; ?>
<title>Update Contact Request status | Dashboard</title>

<body class="skin-black">
    <!-- header logo: style can be found in header.less -->
    <?php include_once '../element/navbar.php'; ?>
    <!-- Header Navbar: style can be found in header.less -->


    <div class="wrapper row-offcanvas row-offcanvas-left">
        <!-- Left side column. contains the logo and sidebar -->

        <!-- Sidebar user panel -->

        <?php include_once '../element/sidebar.php'; ?>

        <!-- /.sidebar -->




        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Update Contact Request status
                    <small>Control panel</small>
                </h1>
                <!--                    <ol class="breadcrumb">
                                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                                        <li class="active">Dashboard</li>
                                    </ol>-->
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Small boxes (Stat box) -->
                <div class="row">

                    <div class="col-lg-6 col-lg-offset-3">
                        <div class="panel panel-default">
                            <div class="panel-heading text-center">
                                Update Contact Request status
                            </div>

                            <?php
                            //build query

                            $queryup = "SELECT * FROM contact_request WHERE id = " . $_GET['id'];
                            //excute the query useing php
                            foreach ($db->query($queryup) as $row) {
                                $editDatapart = $row;
                            }
                            ?>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <form  action="<?= APP_PATH ?>contact_request/update_contact_request.php" method="post" enctype="multipart/form-data">
                                            <input type="hidden" id="id" name="id" class="form-control" value="<?= $editDatapart['id']; ?>">




                                            <div class="form-group">
                                                <label for="contact_request">Contact Request Status </label>

                                                <select class="form-control"  name="contact_request_status" id="contact_request" required="required" alt="contact_request">
                                                    <option>Select Contact Request Status</option>
                                                    <option value="Pending">Pending</option>
                                                    <option value="Reject">Reject</option>
                                                    <option value="In Progress">In Progress</option>
                                                    <option value="Complete">Complete</option>

                                                    <?php
                                                    // $sqledit = "SELECT * FROM contact_request";
                                                    //foreach ($db->query($sqledit) as $row1) {
                                                    ?>



                                                    <!--    <option
 
 
                                                    <?php //if ($row1['id'] == $editDatapart['status']) {  ?>
 
                                                                 selected="selected"  
                                                    <?php //}  ?>
 
 
 value="<?php //echo $row1['id'];   ?>"><?php ///echo $row1['status'];   ?> 
                                                 </option>-->
                                                    <?php //}  ?>


                                                </select>


                                            </div>


                                            <button type="submit" class="btn btn-default"> Update Contact Request status</button>
                                            <!--                                            <button type="reset" class="btn btn-default">Clear</button>-->
                                        </form>
                                    </div>
                                </div>
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>


                </div><!-- /.row (main row) -->

            </section><!-- /.content -->

        </aside><!-- /.right-side -->
    </div><!-- ./wrapper -->




    <!--footer part start here-->
    <?php include_once '../element/footer.php'; ?>    
    <!--footer part end here-->
    <!--    datepicker start here-->
    <script>
        $(document).ready(function () {
            $("#recipes_date").datepicker();

        });
    </script> 



























