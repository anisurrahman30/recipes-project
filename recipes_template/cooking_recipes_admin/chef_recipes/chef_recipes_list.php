<!--/* Author : Munira *-->
<?php
session_start();
include_once '../lib/settings.php';
?>
<?php include_once '../lib/connection.php'; ?>


<?php include_once '../element/headPart.php'; ?>
<title> Chef Recipes List | Dashboard</title>

<body class="skin-black">
    <!-- header logo: style can be found in header.less -->
    <?php include_once '../element/navbar.php'; ?>
    <!-- Header Navbar: style can be found in header.less -->


    <div class="wrapper row-offcanvas row-offcanvas-left">
        <!-- Left side column. contains the logo and sidebar -->

        <!-- Sidebar user panel -->

        <?php include_once '../element/sidebar.php'; ?>

        <!-- /.sidebar -->




        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Chef Recipes List
                    <small>Control panel</small>
                </h1>
                <!--                    <ol class="breadcrumb">
                                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                                        <li class="active">Dashboard</li>
                                    </ol>-->
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Small boxes (Stat box) -->
                <div class="row">

                    <div class="col-lg-12 ">
                        <div class="panel panel-default">
                            <div class="panel-heading text-center" >
                                <strong> <h4> Chef Recipes List</h4></strong>
                            </div>
                            <?php include '../element/msg.php'; ?>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <table width="100%" class="table table-striped table-bordered table-hover table-responsive">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Chef Name</th>
                                                    <th>Recipes Name</th>
                                                    <th>Chef Recipes status</th>
                                                    <th>Chef Recipes Add Date</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $sql = "SELECT cr.*,
                                                        cr.id,
                                                        r.name as recipes_name,
                                                        cd.name as chef_name

                                                        FROM `chef_recipes` as cr 
                                                        LEFT JOIN recipes as r ON cr.`recipes_id`=r.id
                                                        LEFT JOIN chef_detail as cd ON cr.chef_id=cd.id ORDER BY id DESC";


                                                //bulid query table No Record found
                                                if (count($db->query($sql)) == 0) {
                                                    ?>
                                                    <tr>
                                                        <td colspan="6">
                                                            <h3>No Record found</h3>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                                <?php foreach ($db->query($sql) as $sqlresult): ?>
                                                    <!--  bulid query table No Record found end-->


                                                    <tr>
                                                        <td><?php echo $sqlresult['id']; ?></td>
                                                        <td><?php echo $sqlresult['chef_name']; ?></td>


                                                        <td><?php echo $sqlresult['recipes_name']; ?></td>
                                                       <td><?php echo ($sqlresult['is_active']) ? 'active' : 'inactive'; //if else sorthand     ?></td>
                                                        <td><?php echo $sqlresult['date']; ?></td>

                                                        <td>
                                                            <a href="<?= APP_PATH ?>chef_recipes/edit_chef_recipes.php?id=<?php echo $sqlresult['id']; ?>" class="btn btn-primary" role="button">Edit</a> 
                                                            <a href="<?= APP_PATH ?>chef_recipes/delete_chef_recipes.php?id=<?php echo $sqlresult['id']; ?>" class="btn btn-primary" role="button">Delete</a> 
<!--                                                            <a href=">recipes/view_recipes.php?id=<?php// echo $sqlresult['id']; ?>" class="btn btn-primary" role="button">View</a> -->
                                                        </td>

                                                    </tr>
                                                    <?php
                                                endforeach;
                                                //}
                                                ?>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>


                </div><!-- /.row (main row) -->

            </section><!-- /.content -->

        </aside><!-- /.right-side -->
    </div><!-- ./wrapper -->




    <!--footer part start here-->
    <?php include_once '../element/footer.php'; ?>    
    <!--footer part end here-->



























