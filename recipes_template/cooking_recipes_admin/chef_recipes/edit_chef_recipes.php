<!--/* Author : Munira *-->

<?php
session_start();
include_once '../lib/settings.php';
?>
<?php include_once '../lib/connection.php'; ?>


<?php include_once '../element/headPart.php'; ?>
<title>Update Chef Recipes | Dashboard</title>

<body class="skin-black">
    <!-- header logo: style can be found in header.less -->
    <?php include_once '../element/navbar.php'; ?>
    <!-- Header Navbar: style can be found in header.less -->


    <div class="wrapper row-offcanvas row-offcanvas-left">
        <!-- Left side column. contains the logo and sidebar -->

        <!-- Sidebar user panel -->

        <?php include_once '../element/sidebar.php'; ?>

        <!-- /.sidebar -->




        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Update  Chef Recipes
                    <small>Control panel</small>
                </h1>
                <!--                    <ol class="breadcrumb">
                                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                                        <li class="active">Dashboard</li>
                                    </ol>-->
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Small boxes (Stat box) -->
                <div class="row">

                    <div class="col-lg-6 col-lg-offset-3">
                        <div class="panel panel-default">
                            <div class="panel-heading text-center">
                                Update  Chef Recipes
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <?php
                                    $sql = "SELECT * FROM chef_recipes WHERE id='" . $_GET['id'] . "'";
                                    foreach ($db->query($sql) as $row) {
                                        $editpart = $row;
                                    }
                                    ?>
                                    <div class="col-lg-12">
                                        <form  action="<?= APP_PATH ?>chef_recipes/update_chef_recipes.php" method="post">
                                            <input type="hidden" id="id" name="id" class="form-control" value="<?= $editpart['id']; ?>">
                                            
                                             <div class="form-group">
                                                <label for="chef_name">Chef Name</label>
                                                <select  class="form-control" name="chef_name" id="chef_name" required="required" autofocus="autofocus" alt="chef_name" placeholder="Enter Chef Name Here">
                                                    <option>Select Chef</option>


                                                    <?php
                                                    $sqledit = "SELECT * FROM  chef_detail";
                                                    foreach ($db->query($sqledit) as $editrow) {
                                                        ?>
                                                        <option 
                                                        <?php
                                                        if ($editrow['id'] == $editpart['chef_id']) {
                                                            ?>
                                                                selected="selected"

                                                            <?php } ?>

                                                            value="<?php echo $editrow['id']; ?>"><?php echo $editrow['name']; ?>
                                                        </option>
                                                    <?php } ?>
                                                </select>


                                            </div>



                                            <div class="form-group">
                                                <label for="recipes_name">Recipes name</label>

                                                <select  class="form-control" type="text" name="recipes_name" id="recipes_name_video" required="required" alt="recipes_name">
                                                    <option>Select Recipes</option>
                                                    <?php
                                                    $sql = "SELECT * FROM recipes ORDER BY id DESC";
                                                    foreach ($db->query($sql) as $row) {
                                                        ?>
                                                        <option
                                                        <?php
                                                        if ($row['id'] == $editpart['recipes_id']) {
                                                            ?>
                                                                selected="selected"
                                                            <?php } ?>
                                                            value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?>
                                                        </option>
                                                    <?php } ?>
                                                </select>




                                            </div>


                                            <div class="form-group">
                                                <label for="chef_recipes_date"> Chef Recipes Add Date</label>
                                                <input class="form-control" type="date" name="chef_recipes_date" id="date_picker" value="<?php echo $row['date']; ?>" required="required" alt="chef_recipes_date" placeholder="Enter Chef Recipes Submit Date Here    YYYY-MM-DD">

                                            </div>



                                            <div class="form-group">
                                                <label> Chef Recipes  status  is active/inactive now </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="chef_recipes_satus" id="chef_recipes_satus_active" <?php if ($row['is_active'] == 1) { ?>checked="checked"<?php } ?>> Yes
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="chef_recipes_satus" id="chef_recipes_status_active" <?php if ($row['is_active'] == 0) { ?>checked="checked"<?php } ?>> No
                                                </label>
                                            </div>

                                            <button type="submit" class="btn btn-default">Update Chef Recipes</button>
                                            <button type="reset" class="btn btn-default">Clear</button>
                                        </form>
                                    </div>
                                </div>
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>


                </div><!-- /.row (main row) -->

            </section><!-- /.content -->

        </aside><!-- /.right-side -->
    </div><!-- ./wrapper -->




    <!--footer part start here-->
    <?php include_once '../element/footer.php'; ?>    
    <!--footer part end here-->
    <!--    datepicker start here-->



























