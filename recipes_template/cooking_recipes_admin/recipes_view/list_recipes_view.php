<!--/* Author : Munira *-->

<?php 
session_start();
include_once '../lib/settings.php'; 
?>
<?php include_once '../lib/connection.php'; ?>


<?php include_once '../element/headPart.php'; ?>
<title> Recipes View List| Dashboard</title>

<body class="skin-black">
    <!-- header logo: style can be found in header.less -->
    <?php include_once '../element/navbar.php'; ?>
    <!-- Header Navbar: style can be found in header.less -->


    <div class="wrapper row-offcanvas row-offcanvas-left">
        <!-- Left side column. contains the logo and sidebar -->

        <!-- Sidebar user panel -->

        <?php include_once '../element/sidebar.php'; ?>

        <!-- /.sidebar -->




        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                   Recipes View List
                    <small>Control panel</small>
                </h1>
                <!--                    <ol class="breadcrumb">
                                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                                        <li class="active">Dashboard</li>
                                    </ol>-->
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Small boxes (Stat box) -->
                <div class="row">

                    <div class="col-lg-12 ">
                        <div class="panel panel-default">
                            <div class="panel-heading text-center" >
                                <strong> <h4>  Recipes View List</h4></strong>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <table width="100%" class="table table-striped table-bordered table-hover table-responsive">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Recipes  Name</th>
                                                    <th>Recipes Viewed</th>
<!--                                                    <th>User IP</th>
                                                    <th>Recipes View Date</th>-->

<!--                                                    <th>Action</th>-->
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $sql = "select r.*,
                                                        r.name,
                                                        rv.user_ip,
                                                        count(rv.id) as total_viewer from recipes as r 
                                                         LEFT JOIN recipes_view as rv ON r.id=rv.recipes_id 
                                                         GROUP BY r.id
                                                                                                                     ";
                                                foreach ($db->query($sql) as $sqlresult):
                                                    ?>

                                                    <tr>
                                                        <td><?php echo $sqlresult['id']; ?></td>
                                                        <td><?php echo $sqlresult['name']; ?></td>
                                                        <td><?php echo $sqlresult['total_viewer']; ?></td>
<!--                                                        <td><?php //echo $sqlresult['user_ip']; ?></td>
                                                        <td><?php //echo $sqlresult['date']; ?></td>-->



                                                    </tr>
                                                    <?php
                                                endforeach;
                                                //}
                                                ?>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>


                </div><!-- /.row (main row) -->

            </section><!-- /.content -->

        </aside><!-- /.right-side -->
    </div><!-- ./wrapper -->




    <!--footer part start here-->
    <?php include_once '../element/footer.php'; ?>    
    <!--footer part end here-->



























