<!--/* Author : Munira *-->
<?php
session_start();
include_once '../lib/settings.php';
?>
<?php include_once '../lib/connection.php'; ?>


<?php include_once '../element/headPart.php'; ?>
<title> Recipes List | Dashboard</title>

<body class="skin-black">
    <!-- header logo: style can be found in header.less -->
    <?php include_once '../element/navbar.php'; ?>
    <!-- Header Navbar: style can be found in header.less -->


    <div class="wrapper row-offcanvas row-offcanvas-left">
        <!-- Left side column. contains the logo and sidebar -->

        <!-- Sidebar user panel -->

        <?php include_once '../element/sidebar.php'; ?>

        <!-- /.sidebar -->




        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Recipes List
                    <small>Control panel</small>
                </h1>
                <!--                    <ol class="breadcrumb">
                                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                                        <li class="active">Dashboard</li>
                                    </ol>-->
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Small boxes (Stat box) -->
                <div class="row">
                    
                    <div class="col-lg-12 ">
                        <div class="panel panel-default">
                            <div class="panel-heading text-center" >
                                <strong> <h4>  Recipes List</h4></strong>
                            </div>
                            <?php include '../element/msg.php'; ?>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <table width="100%" class="table table-striped table-bordered table-hover table-responsive">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Recipes Name</th>
                                                    <th>Recipes Category Name</th>
                                                    <th>Recipes Ingredients</th>
                                                    <th>Recipes Directions</th>
                                                    <th>Recipes Image</th>

                                                    <th>Recipes is featured</th>
                                                    <th>Recipes is slider</th>
                                                    <th>Recipes status</th>
                                                    <th>Recipes Date</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $sql = "SELECT * FROM recipes ORDER BY id DESC";
                                               

                                                    //bulid query table No Record found
                                                    if (count($db->query($sql)) == 0) {
                                                        ?>
                                                        <tr>
                                                            <td colspan="6">
                                                                <h3>No Record found</h3>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                        <?php  foreach ($db->query($sql) as $sqlresult):?>
                                                    <!--  bulid query table No Record found end-->


                                                    <tr>
                                                        <td><?php echo $sqlresult['id']; ?></td>
                                                        <td><?php echo $sqlresult['name']; ?></td>
                                                        

                                                        <td><?php echo $sqlresult['ingredients']; ?></td>
                                                        <td><?php echo $sqlresult['directions']; ?></td>
                                                        <td> <img src="<?= APP_PATH ?>images/<?php echo $sqlresult['image']; ?>" style="width:300px;height:228px; border:2px inset black;" class="img-thumbnail img-responsive"></td>
                                                        <td><?php echo ($sqlresult['is_featured']) ? 'active' : 'inactive'; //if else sorthand    ?></td>
                                                        <td><?php echo ($sqlresult['is_slider']) ? 'active' : 'inactive'; //if else sorthand    ?></td>

                                                        <td><?php echo ($sqlresult['is_active']) ? 'active' : 'inactive'; //if else sorthand   ?></td>
                                                        <td><?php echo $sqlresult['date']; ?></td>

                                                        <td>
                                                            <a href="<?= APP_PATH ?>recipes/edit_recipes.php?id=<?php echo $sqlresult['id']; ?>" class="btn btn-primary" role="button">Edit</a> 
                                                            <a href="<?= APP_PATH ?>recipes/delete_recipes.php?id=<?php echo $sqlresult['id']; ?>" class="btn btn-primary" role="button">Delete</a> 
                                                            <a href="<?= APP_PATH ?>recipes/view_recipes.php?id=<?php echo $sqlresult['id']; ?>" class="btn btn-primary" role="button">View</a> 
                                                        </td>

                                                    </tr>
                                                    <?php
                                                endforeach;
                                                //}
                                                ?>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>


                </div><!-- /.row (main row) -->

            </section><!-- /.content -->

        </aside><!-- /.right-side -->
    </div><!-- ./wrapper -->




    <!--footer part start here-->
    <?php include_once '../element/footer.php'; ?>    
    <!--footer part end here-->



























