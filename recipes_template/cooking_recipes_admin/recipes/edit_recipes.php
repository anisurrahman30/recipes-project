<!--/* Author : Munira *-->
<?php
session_start();
include_once '../lib/settings.php';
?>
<?php include_once '../lib/connection.php'; ?>


<?php include_once '../element/headPart.php'; ?>
<title>Update Recipes | Dashboard</title>

<body class="skin-black">
    <!-- header logo: style can be found in header.less -->
    <?php include_once '../element/navbar.php'; ?>
    <!-- Header Navbar: style can be found in header.less -->


    <div class="wrapper row-offcanvas row-offcanvas-left">
        <!-- Left side column. contains the logo and sidebar -->

        <!-- Sidebar user panel -->

        <?php include_once '../element/sidebar.php'; ?>

        <!-- /.sidebar -->




        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Dashboard
                    <small>Control panel</small>
                </h1>
                <!--                    <ol class="breadcrumb">
                                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                                        <li class="active">Dashboard</li>
                                    </ol>-->
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Small boxes (Stat box) -->
                <div class="row">

                    <div class="col-lg-6 col-lg-offset-3">
                        <div class="panel panel-default">
                            <div class="panel-heading text-center">
                                Updated Recipes
                            </div>

                            <?php
                            //build query

                            $queryup = "SELECT * FROM recipes WHERE id = " . $_GET['id'];
                            //excute the query useing php
                            foreach ($db->query($queryup) as $row) {
                                $editDatapart = $row;
                            }
                            ?>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <form  action="<?= APP_PATH ?>recipes/update_recipes.php" method="post" enctype="multipart/form-data">
                                            <input type="hidden" id="id" name="id" class="form-control" value="<?= $editDatapart['id']; ?>">
                                            <div class="form-group">
                                                <label for="recipesName">Recipes Name</label>
                                                <input class="form-control" type="text" name="recipesName" id="recipes_name" value="<?= $editDatapart['name']; ?>" required="required" autofocus="autofocus" alt="recipesName" placeholder="Enter Recipes Name Here">
                                                <p class="help-block"></p>
                                            </div>



                                            <div class="form-group">
                                                <label for="recipes_categorys">Recipes Category </label>

                                                <select class="form-control"  name="recipes_categorys" id="recipes_categorys" required="required" alt="recipes_categorys">
                                                    <option>Select Category</option>
                                                    <?php
                                                    $sqledit = "SELECT * FROM recipes_category";
                                                    foreach ($db->query($sqledit) as $row1) {
                                                        ?>



                                                        <option    
                                                            <?php 
                                                            if ($row1['id'] == $editDatapart['category_id']) { ?>

                                                                selected="selected"  
                                                            <?php } ?>
                                                                
                                                                
                                                            value="<?php echo $row1['id']; ?>"><?php echo $row1['name']; ?> </option>
                                                        <?php } ?>


                                                </select>


                                            </div>

                                            <div class="form-group">
                                                <label for="recipes_ingredients">Recipes Ingredients </label>

                                                <textarea class="form-control"  name="recipes_ingredients" id="recipes_ingredients" required="required" alt="recipes_ingredients" placeholder="Enter Recipes Ingredients  Here" rows="5"><?= $editDatapart['ingredients']; ?></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="recipes_directions">Recipes Directions </label>
                                                <textarea class="form-control"  name="recipes_directions" id="recipes_directions" required="required" alt="recipes_directions" placeholder="Enter Recipes directions  Here"><?= $editDatapart['directions']; ?></textarea>

                                            </div>





                                            <div class="form-group">
                                                <label for="recipes_date">Recipes Date</label>
                                                <input class="form-control" type="date" name="recipes_date" id="recipes_date" value="<?= $editDatapart['date']; ?>" required="required" alt="recipes_date" placeholder="Enter Recipes submit Date Here    YYYY-MM-DD">

                                            </div>
                                            <div class="form-group">
                                                <label for="recipes_image">Recipes Image</label><br>

                                                <img src="<?= APP_PATH ?>images/<?php echo $editDatapart['image']; ?>"  alt="recipes_image"  style="width:304px;height:228px; border:8px inset black;" class="img-thumbnail img-responsive"><br><br>

                                                <input type="file" name="recipes_image" class="form-control-file" id="recipes_image"  aria-describedby="fileHelp" alt="recipes_image" ><br>
                                                <input type="hidden" value="<?php echo $editDatapart['image']; ?>" name="ex_chef_image" />

<!--                                                <input  type="file" name="recipes_image" id="recipes_image" value="recipes_image" required="required" alt="recipes_image" placeholder="Enter Recipes Image Here">-->

                                            </div>


                                            <div class="form-group">
                                                <label>Recipes is featured now</label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="featured" <?php if ($editDatapart['is_featured'] == 1) { ?> checked="checked"  <?php } ?> id="featured_active" > Yes
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="featured" <?php if ($editDatapart['is_featured'] == 0) { ?> checked="checked"  <?php } ?> id="featured_inactive"> No
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label>Recipes is slider now</label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="slider" id="slider_active" <?php if ($editDatapart['is_slider'] == 1) { ?> checked="checked"  <?php } ?>> Yes
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="slider" id="slider_inactive" <?php if ($editDatapart['is_slider'] == 0) { ?> checked="checked"  <?php } ?>> No
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label>Recipes status  is active/inactive now</label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="status" id="status_active" <?php if ($editDatapart['is_active'] == 1) { ?> checked="checked"  <?php } ?>> Yes
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="status" id="status_inactive" <?php if ($editDatapart['is_active'] == 0) { ?> checked="checked"  <?php } ?>> No
                                                </label>
                                            </div>
                                            <button type="submit" class="btn btn-default">Update Recipes</button>
                                            <!--                                            <button type="reset" class="btn btn-default">Clear</button>-->
                                        </form>
                                    </div>
                                </div>
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>


                </div><!-- /.row (main row) -->

            </section><!-- /.content -->

        </aside><!-- /.right-side -->
    </div><!-- ./wrapper -->




    <!--footer part start here-->
    <?php include_once '../element/footer.php'; ?>    
    <!--footer part end here-->
    <!--    datepicker start here-->
    <script>
        $(document).ready(function () {
            $("#recipes_date").datepicker();

        });
    </script> 



























