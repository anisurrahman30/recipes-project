<!--/* Author : Munira *-->
<?php 
session_start();
include_once '../lib/settings.php'; 

?>
<?php include_once '../lib/connection.php'; ?>


<?php include_once '../element/headPart.php'; ?>
<title>Add Recipes | Dashboard</title>

<body class="skin-black">
    <!-- header logo: style can be found in header.less -->
    <?php include_once '../element/navbar.php'; ?>
    <!-- Header Navbar: style can be found in header.less -->


    <div class="wrapper row-offcanvas row-offcanvas-left">
        <!-- Left side column. contains the logo and sidebar -->

        <!-- Sidebar user panel -->

        <?php include_once '../element/sidebar.php'; ?>

        <!-- /.sidebar -->




        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Add Recipes
                    <small>Control panel</small>
                </h1>
                <!--                    <ol class="breadcrumb">
                                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                                        <li class="active">Dashboard</li>
                                    </ol>-->
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Small boxes (Stat box) -->
                <div class="row">

                    <div class="col-lg-6 col-lg-offset-3">
                        <div class="panel panel-default">
                            <div class="panel-heading text-center">
                                Add Recipes
                            </div>
                            <div class="panel-body">
                                  <?php include '../element/msg.php'; ?>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <form  action="<?= APP_PATH ?>recipes/action_recipes.php" method="post" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <label for="recipesName">Recipes Name</label>
                                                <input class="form-control" type="text" name="recipesName" id="recipes_name" required="required" autofocus="autofocus" alt="recipesName" placeholder="Enter Recipes Name Here">
                                                <p class="help-block"></p>
                                            </div>
                                            <?php
                                            $sqls = "SELECT * FROM recipes_category ORDER BY id DESC";
                                            ?>
                                            <div class="form-group">
                                                <label for="recipes_categorys">Recipes Category </label>

                                                <select class="form-control"  name="recipes_categorys" id="recipes_categorys" required="required" alt="recipes_categorys">
                                                      <option>Select Category</option>
                                                    <?php
                                                    foreach ($db->query($sqls) as $row) {
                                                        ?>
                                                  
                                                        <option value="<?php echo $row['category_id']; ?>"><?php echo $row['name']; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>


<!--                                                <input   placeholder="Enter Recipes Ingredients  Here" rows="5">-->
                                            </div>

                                            <div class="form-group">
                                                <label for="recipes_ingredients">Recipes Ingredients </label>

                                                <textarea class="form-control"  name="recipes_ingredients" id="recipes_ingredients" required="required" alt="recipes_ingredients" placeholder="Enter Recipes Ingredients  Here" rows="5"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="recipes_directions">Recipes Directions </label>
                                                <textarea class="form-control" type="text" name="recipes_directions" id="recipes_directions" required="required" alt="recipes_directions" placeholder="Enter Recipes directions  Here"></textarea>

                                            </div>





                                            <div class="form-group">
                                                <label for="recipes_date">Recipes Date</label>
                                                <input class="form-control" type="text" name="recipes_date" id="date_picker" required="required" alt="recipes_date" placeholder="Enter Recipes submit Date Here    YYYY-MM-DD">

                                            </div>
                                            <div class="form-group">
                                                <label for="recipes_image">Recipes Image</label>
                                                <input  type="file" name="recipes_image" id="recipes_image" required="required" alt="recipes_image" placeholder="Enter Recipes Image Here">

                                            </div>


                                            <div class="form-group">
                                                <label>Recipes is featured now</label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="featured" id="featured_active" value="1"> Yes
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="featured" id="featured_inactive" value="0"> No
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label>Recipes is slider now</label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="slider" id="slider_active" value="1" > Yes
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="slider" id="slider_inactive" value="0"> No
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label>Recipes status  is active/inactive now</label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="status" id="status_active" value="1"> Yes
                                                </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="status" id="status_inactive" value="0"> No
                                                </label>
                                            </div>
                                            <button type="submit" class="btn btn-default">Add Recipes</button>
                                            <button type="reset" class="btn btn-default">Clear</button>
                                        </form>
                                    </div>
                                </div>
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>


                </div><!-- /.row (main row) -->

            </section><!-- /.content -->

        </aside><!-- /.right-side -->
    </div><!-- ./wrapper -->




    <!--footer part start here-->
    <?php include_once '../element/footer.php'; ?>    
    <!--footer part end here-->
    <!--    datepicker start here-->



























