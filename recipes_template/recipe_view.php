<!--/* Author : Munira *-->
<?php include_once './lib/settings.php'; ?>
<?php include_once './lib/connection.php'; ?>
<?php
include_once './segments/header_segments.php';
$user_ip=$_SERVER['REMOTE_ADDR'];
$sqlpagequery = "INSERT INTO recipes_view SET  recipes_id='".$_GET['rec_id']."',user_ip='$user_ip', date='".date('Y-m-d')."'";



//to excute a query like mysql
$db->exec($sqlpagequery);
?>
<body class="recipePage">
    <!--preloader-->
    <div class="preloader">
        <div class="spinner"></div>
    </div>
    <!--//preloader-->

    <!--header-->
    <header class="head" role="banner">
        <!--wrap-->
        <div class="wrap clearfix">
            <a href="index.php" title="SocialChef" class="logo"><img src="images/ico/logo.png" alt="SocialChef logo" /></a>

            <!--top navbar manus item start here-->
            <?php include_once './segments/top_navbar_menu_item.php'; ?>
            <!--top navbar manus item end here-->
        </div>
        <!--//wrap-->
    </header>
    <!--//header-->

    <!--main-->
    <main class="main" role="main">
        <!--wrap-->
        <div class="wrap clearfix">
            <!--breadcrumbs-->
            <nav class="breadcrumbs">
                <ul>
                    <li><a href="index.php" title="Home">Home</a></li>
                    <!--                    <li><a href="#" title="Recipes">Recipes</a></li>
                                        <li><a href="recipes.html" title="Cocktails">Deserts</a></li>-->
                    <li>Recipe</li>
                </ul>
            </nav>
            <!--//breadcrumbs-->

            <!--row-->
            <div class="row">
                <?php
                $queryup = "SELECT r.*,
                r.name,
                rc.name as category_name
                FROM recipes as r
                LEFT JOIN recipes_category as rc ON r.category_id = rc.id

                WHERE r.id ='" . $_GET['rec_id'] . "'";

                //excute the query useing php
                foreach ($db->query($queryup) as $row) {
                    $recipes = $row;
                }
                ?>
                <header class="s-title">
                    <h1><?php echo $recipes['name']; ?></h1>
                </header>
                <!--content-->
                <section class="content three-fourth">
                    <!--recipe-->
                    <div class="recipe">
                        <div class="row">
                            <!--two-third-->
                            <article class="full-width">
                                <div class="image"><a href="<?= SITE_IMG_PATH ?>recipe_view.php?rec_id=<?= $row['id']; ?>">

                                        <img src="<?= SITE_IMG_PATH ?><?php echo $recipes['image']; ?>" style="height: 550px !important; width:700px;" alt="recipes_image"  />
                                </div>
<!--                                <div class="intro"><p><strong>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium volup.</p></div>-->
                                <div class="instructions">
                                    <ol>
                                        <li><?php echo $recipes['category_name']; ?></li>
                                        <li><?php echo $recipes['ingredients']; ?></li>
                                        <li><?php echo $recipes['directions']; ?></li>

                                    </ol>
                                </div>
                            </article>

                            <!--//two-third-->

                            <!--one-third-->

                            <!--//one-third-->
                        </div>
                    </div>

                </section>

                <!--//content-->

                <!--right sidebar-->

            </div>
            <!--//row-->
        </div>
        <!--//wrap-->
    </main>
    <!--//main-->


    <!--footer-->
    <?php include_once './segments/footer_part.php'; ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


