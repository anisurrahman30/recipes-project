
<?php
$sql_total = "SELECT 
(SELECT count(customer.id) FROM customer WHERE customer.user_type='user') as customer,
(SELECT count(recipes.id) FROM recipes) as recipes,
(SELECT count(recipes.id) FROM recipes) as recipes_images,
(SELECT count(wishlist.id) FROM wishlist) as wishlist,
(SELECT count(recipes_category.id) FROM recipes_category) as recipes_category,
(SELECT count(customer_recipes_request.id) FROM customer_recipes_request) as customer_recipes_request,
(SELECT count(chef_detail.id) FROM chef_detail) as chef_detail";
$res_total = $db->query($sql_total);
foreach($res_total as $row):
?>
<section class="content full-width">
    <div class="icons dynamic-numbers">
        <header class="s-title">
            <h2 class="ribbon large">SocialChef in numbers</h2>
        </header>

        <!--row-->
        <div class="row">
            <!--item-->
            <div class="one-sixth">
                <div class="container">
                    <i class="icon icon-themeenergy_chef-hat"></i>
                    <span class="title dynamic-number" data-dnumber="<?=$row['customer']?>"><?=$row['customer']?></span>
                    <span class="subtitle">members</span>
                </div>
            </div>
            <!--//item-->

            <!--item-->
            <div class="one-sixth">
                <div class="container">
                    <i class="icon icon-themeenergy_pan"></i>
                    <span class="title dynamic-number" data-dnumber="<?=$row['recipes']?>"><?=$row['recipes']?></span>
                    <span class="subtitle">recipes</span>
                </div>
            </div>
            <!--//item-->

            <!--item-->
            <div class="one-sixth">
                <div class="container">
                    <i class="icon icon-themeenergy_image"></i>
                    <span class="title dynamic-number" data-dnumber="<?=$row['recipes_images']?>"><?=$row['recipes_images']?></span>
                    <span class="subtitle">photos</span>
                </div>
            </div>
            <!--//item-->

            <!--item-->
            <div class="one-sixth">
                <div class="container">
                    <i class="icon icon-themeenergy_pencil"></i>
                    <span class="title dynamic-number" data-dnumber="<?=$row['recipes_category']?>"><?=$row['recipes_category']?></span>
                    <span class="subtitle">category</span>
                </div>
            </div>
            <!--//item-->

            <!--item-->
            <div class="one-sixth">
                <div class="container">
                    <i class="icon icon-themeenergy_chat-bubbles"></i>
                    <span class="title dynamic-number" data-dnumber="<?=$row['chef_detail']?>"><?=$row['chef_detail']?></span>
                    <span class="subtitle">chef</span>
                </div>
            </div>
            <!--//item-->

            <!--item-->
            <div class="one-sixth">
                <div class="container">
                    <i class="icon icon-themeenergy_stars"></i>
                    <span class="title dynamic-number" data-dnumber="<?=$row['customer_recipes_request']?>"><?=$row['customer_recipes_request']?></span>
                    <span class="subtitle">request</span>
                </div>
            </div>
            <!--//item-->

            <div class="cta">
                <a href="<?= APP_PATH ?>login.php" class="button big">Join us!</a>
            </div>
        </div>
        <!--//row-->
    </div>
</section>
<?php endforeach; ?>