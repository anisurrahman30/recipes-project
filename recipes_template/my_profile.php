<!--/* Author : Munira *-->
<?php include_once './lib/settings.php'; ?>
<?php include_once './lib/connection.php'; ?>
<?php include_once './segments/header_segments.php'; ?>
<body>
    <!--preloader-->
    <div class="preloader">
        <div class="spinner"></div>
    </div>
    <!--//preloader-->

    <!--header-->
    <header class="head" role="banner">
        <!--wrap-->
        <div class="wrap clearfix">
            <a href="index.html" title="SocialChef" class="logo"><img src="images/ico/logo.png" alt="SocialChef logo" /></a>

            <!--top navbar manus item start here-->
            <?php include_once './segments/top_navbar_menu_item.php'; ?>
            <!--top navbar manus item end here-->


        </div>
    </header>
    <!--//header-->

    <!--main-->
    <main class="main" role="main">
        <!--wrap-->
        <div class="wrap clearfix">
            <!--breadcrumbs-->
            <nav class="breadcrumbs">
                <ul>
                    <li><a href="index.php" title="Home">Home</a></li>
                    <li>My account</li>
                </ul>
            </nav>
            <!--//breadcrumbs-->


            <!--content-->
            <section class="content">
                <!--row-->
                <div class="row">
                    <!--profile left part-->
                    <div class="my_account one-fourth">
                        <figure>
                            <img src="<?= SITE_IMG_PATH ?><?php echo['image'];?>" alt="user_image" style="height: 190px !important; width:280px;" />
                        </figure>
                        <div class="container">
                            <h2>Anabelle Q.</h2> 
                        </div>
                    </div>
                    <!--//profile left part-->

                    <div class="three-fourth">
                        <nav class="tabs">
                            <ul>
                                <li class="active"><a href="#about" title="About me">About me</a></li>
                                <li><a href="#recipes" title="My recipes">My recipes</a></li>
                                <li><a href="#wishlist" title="Wishlist ">My Wishlist</a></li>
                                <li><a href="#approved" title="My Approve Recipes">Approved</a></li>
                            </ul>
                        </nav>

                        <!--about-->
                        <div class="tab-content" id="about">
                            <?php
                            $sql = "SELECT c.`name` as user_name,c.`email` as user_email,c.`date`,c.`status` FROM `customer` as c WHERE id='$customer_id'";
                            foreach ($db->query($sql) as $row):
                                $profile = $row;
                                ?>
                                <div class="entries row">
                                    <dl class="basic four-col">
                                        <dt>Name</dt>
                                        <dd><?php echo $profile['user_name']; ?></dd>
                                        <dt>Email</dt>
                                        <dd><?php echo $profile['user_email']; ?></dd>

                                    </dl>

                                    <dl class="basic four-col">
                                        <dt>Date</dt>
                                        <dd><?php
                                            if (!empty($profile['date'])) {
                                                echo $profile['date'];
                                            } else {
                                                echo "Not Mention";
                                            }
                                            ?></dd>
                                        <dt>Status</dt>
                                        <dd><?php echo $profile['status']; ?></dd>

                                    </dl>


                                </div>
                            <?php endforeach; ?>
                        </div>
                        <!--//about-->




                        <!--my recipes-->
                        <div class="tab-content" id="recipes">

                            <div class="entries row">

                                <?php
                                $sql = "SELECT
                                        crr.id,
                                        crr.image as recipes_image,
                                        crr.name as recipes_request_name
                                        FROM `customer_recipes_request`  as crr
                                        WHERE crr.`customer_id`='$customer_id' 
                                        ORDER BY crr.id DESC";
                                foreach ($db->query($sql) as $row) :
                                    ?>

                                    <!--item-->
                                    <div class="entry one-third">
                                        <figure>
                                            <img src="<?= APP_PATH ?>ajax/uploads/<?php echo $row['recipes_image']; ?>" style="height: 190px !important; width:280px;" alt="<?php echo $row['recipes_request_name']; ?>" />
                                        </figure>
                                        <div class="container">
                                            <h2><a href="#"><?php echo $row['recipes_request_name']; ?></a></h2> 
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                                <!--item-->


                            </div>
                        </div>


                        <div class="tab-content" id="wishlist">

                            <div class="entries row">

                                <?php
                                $sql = "SELECT 
                                        w.`customer_id`,
                                        r.*,
                                        r.id as rec_id FROM `wishlist` as w
                                        INNER JOIN recipes as r ON w.`recipes_id`=r.id 
                                        WHERE `customer_id`='2'";
                                        foreach ($db->query($sql) as $row) :
                                    ?>

                                    <!--item-->
                                    <div class="entry one-third">
                                        <figure>
                                            <img src="<?= SITE_IMG_PATH ?><?php echo $row['image']; ?>" style="height: 190px !important; width:280px;" alt="<?php echo $row['name']; ?>" />
                                        </figure>
                                        <div class="container">
                                            <h2><a href="#"><?php echo $row['name']; ?></a></h2> 
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                                <!--item-->


                            </div>
                        </div>



                        <div class="tab-content" id="approved">

                            <div class="entries row">

                                <?php
                                $sql = "SELECT
                                        crr.id,
                                        crr.image as recipes_image,
                                        crr.name as recipes_request_name
                                        FROM `customer_recipes_request`  as crr
                                        WHERE crr.`customer_id`='$customer_id'   AND crr.`status`='Approve'
                                        ORDER BY crr.id DESC";
                                foreach ($db->query($sql) as $row) :
                                    ?>

                                    <!--item-->
                                    <div class="entry one-third">
                                        <figure>
                                            <img src="<?= APP_PATH ?>ajax/uploads/<?php echo $row['recipes_image']; ?>" style="height: 190px !important; width:280px;" alt="<?php echo $row['recipes_request_name']; ?>" />
                                        </figure>
                                        <div class="container">
                                            <h2><a href="#"><?php echo $row['recipes_request_name']; ?></a></h2> 
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                                <!--item-->


                            </div>
                        </div>
                        <!--//my recipes-->


                        <!--my favorites-->
                        <div class="tab-content" id="favorites">
                            <div class="entries row">
                                <!--item-->
                                <div class="entry one-third">
                                    <figure>
                                        <img src="<?= SITE_IMG_PATH ?>images/<?php echo $row['image']; ?>" style="height: 190px !important; width:280px;" alt="recipes_image" />
                                        <figcaption><a href="recipe.php"><i class="icon icon-themeenergy_eye2"></i> <span>View recipe</span></a></figcaption>
                                    </figure>
                                    <div class="container">
                                        <h2><a href="recipe.html">Thai fried rice with fruit and vegetables</a></h2> 
                                        <div class="actions">
                                            <div>
                                                <div class="difficulty"><i class="ico i-medium"></i><a href="#">medium</a></div>
                                                <div class="likes"><i class="fa fa-heart"></i><a href="#">10</a></div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--item-->

                            </div>
                        </div>
                        <!--//my favorites-->

                        <!--my posts-->

                        <!--//my posts-->
                    </div>
                </div>
                <!--//row-->
            </section>
            <!--//content-->
        </div>
        <!--//wrap-->
    </main>
    <!--//main-->


    <!--footer-->
    <?php include_once './segments/footer_part.php'; ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!--//footer end-->



