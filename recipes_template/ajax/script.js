$(function ()
{
    // Variable to store your files
    var files;

    // Add events
    $('input[type=file]').on('change', prepareUpload);
    $('form').on('submit', uploadFiles);

    // Grab the files and set them to our variable
    function prepareUpload(event)
    {
        files = event.target.files;
    }

    // Catch the form submit and upload the files
    function uploadFiles(event)
    {
        event.stopPropagation(); // Stop stuff happening
        event.preventDefault(); // Totally stop stuff happening

        // START A LOADING SPINNER HERE

        // Create a formdata object and add the files
        var data = new FormData();
        $.each(files, function (key, value)
        {
            data.append(key, value);
        });

        $.ajax({
            url: 'ajax/submit.php?files',
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function (data, textStatus, jqXHR)
            {
                if (typeof data.error === 'undefined')
                {
                    // Success so call function to process the form
                    submitForm(event, data);
                } else
                {
                    // Handle errors here
                    console.log('ERRORS: ' + data.error);
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                // Handle errors here
                console.log('ERRORS: ' + textStatus);
                // STOP LOADING SPINNER
            }
        });
    }

    function submitForm(event, data)
    {
        // Create a jQuery object from the form
        
        var name = $("input[name='name']").val();
        var ingredients = $("textarea[name='ingredients']").val();
        var directions = $("textarea[name='directions']").val();

        if (name == '' || ingredients == '' || directions == '')
        {
            $("#msg_place").html(showMessage('alert', 'Please Fillup All Fields.'));
            $("html, body").animate({scrollTop: 0}, "slow");
        } else
        {
            var imgdata=data;

            $.post("./ajax/recipies_request.php", {'name': name, 'ingredients': ingredients, 'directions': directions, 'img_data': imgdata}, function (data) {
                //alert(data);
                var obj = jQuery.parseJSON(data);
                if (obj.status == 1)
                {
                    $("#msg_place").html(showMessage('succees', 'Recipies Request Successfully Saved.'));
                    $("input[name='name']").val("");
                    $("textarea[name='ingredients']").val("");
                    $("textarea[name='directions']").val("");
                    $("html, body").animate({scrollTop: 0}, "slow");
                } 
                else if (obj.status == 2)
                {
                    $("#msg_place").html(showMessage('alert', 'Please Select a Image.'));
                    $("html, body").animate({scrollTop: 0}, "slow");
                }
                else
                {
                    $("#msg_place").html(showMessage('alert', 'Recipies Request Failed TO Save, Please Try Again.'));
                    $("html, body").animate({scrollTop: 0}, "slow");
                }
            });

        }
    }
});