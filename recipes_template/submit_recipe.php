<!--/* Author : Munira *-->
<?php include_once './lib/settings.php'; ?>
<?php include_once './lib/connection.php'; ?>
<?php include_once './segments/header_segments.php'; ?>
<body>
    <!--preloader-->
    <div class="preloader">
        <div class="spinner"></div>
    </div>
    <!--//preloader-->

    <!--header-->
    <header class="head" role="banner">
        <!--wrap-->
        <div class="wrap clearfix">
            <a href="index.php" title="SocialChef" class="logo"><img src="images/ico/logo.png" alt="SocialChef logo" /></a>

            <!--top navbar manus item start here-->
            <?php include_once './segments/top_navbar_menu_item.php'; ?>
            <!--top navbar manus item end here-->
        </div>
    </header>
    <!--//header-->

    <!--main-->
    <main class="main" role="main">
        <!--wrap-->
        <div class="wrap clearfix">
            <!--breadcrumbs-->
            <nav class="breadcrumbs">
                <ul>
                    <li><a href="index.html" title="Home">Home</a></li>
                    <li>Submit a recipe</li>
                </ul>
            </nav>
            <!--//breadcrumbs-->

            <!--row-->
            <div class="row">
                <header class="s-title">
                    <h1>Add a new recipe</h1>
                </header>

                <!--content-->
                <section class="content full-width">
                    <div id="msg_place"></div>
                    <div class="submit_recipe container">
                        <form action="#" enctype="multipart/form-data" method="post">
                            <section>
                                <h2>Please share your recipies</h2>
                                <p>All fields are required.</p>
                                <div class="f-row">
                                    <div class="full"><input type="text" name="name" placeholder="Recipe Name" /></div>
                                </div>
                                <div class="f-row">
                                    <div class="full"><input type="file" name="file_upload" id="file_upload" /></div>
                                </div>
<!--                                <div class="f-row">
                                    <div class="third"><input type="text" placeholder="Preparation time" /></div>
                                    <div class="third"><input type="text" placeholder="Cooking time" /></div>
                                    <div class="third"><input type="text" placeholder="Difficulty" /></div>
                                </div>-->
<!--                                <div class="f-row">
                                    <div class="third"><input type="text" placeholder="Serves how many people?" /></div>
                                    <div class="third"><select><option selected="selected">Select a category</option></select></div>
                                </div>-->
                            </section>

                            <section>
                                <h2>Ingredients</h2>
                                <div class="f-row">
                                    <div class="full"><textarea name="ingredients" placeholder="Recipe Ingredients"></textarea></div>
                                </div>
                            </section>
                            <section>
                                <h2>Directions</h2>
                                <div class="f-row">
                                    <div class="full"><textarea name="directions" placeholder="Recipe Directions"></textarea></div>
                                </div>
                            </section>

                            

                            <div class="f-row full">
                                <input  type="submit" name="submit" class="button" id="submitRecipe" value="Submit recipeis request" />
                            </div>
                        </form>
                    </div>
                </section>
                <!--//content-->
            </div>
            <!--//row-->
        </div>
        <!--//wrap-->
    </main>
    <!--//main-->

    <!--footer-->
    <?php include_once './segments/footer_part.php'; ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="ajax/script.js"></script>
    <script src="ajax/recipies_request.js"></script>


