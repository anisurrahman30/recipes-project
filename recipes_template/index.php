<!--/* Author : Munira *-->
<?php include_once './lib/settings.php'; ?>
<?php include_once './lib/connection.php'; ?>
<?php include_once './segments/header_segments.php'; ?>


<body class="home">

    <!--preloader-->
    <div class="preloader">
        <div class="spinner"></div>
    </div>
    <!--//preloader-->

    <!--header-->
    <header class="head" role="banner">
        <!--wrap-->
        <div class="wrap clearfix">

            <a href="<?= APP_PATH ?>index.php" title="SocialChef" class="logo"><img src="images/ico/logo.png" alt="SocialChef logo" /></a>

            <!--top navbar manus item start here-->
            <?php include_once './segments/top_navbar_menu_item.php'; ?>
            <!--top navbar manus item end here-->
        </div>
        <!--//wrap-->

    </header>
    <!--//header-->

    <!--main-->
    <main class="main" role="main"> 


        <!--Slider start here-->
        <?php
        include './segments/full_slider.php';
        ?>
        <!--//Slider start here-->


        <!--wrap-->
        <div class="wrap clearfix">
            <!--row-->
            <div class="row">
                <!--content-->
                <?php include('./total_count_show/total_count_show.php'); ?>
                <!--//content-->

                <!--content-->
                <section class="content full-width">
                    <!--cwrap-->
                    <div class="cwrap">
                        <!--entries-->
                        <div class="entries row">
                            <!--featured recipe-->
                            <?php include_once './recipes_of_the_day/special_reipes_day.php'; ?>
                            <!--//featured recipe-->

                            <!--featured member-->
                            <?php include_once './chef_member/chef_special_recipes.php'; ?>
                            <!--//featured member-->
                        </div>
                        <!--//entries-->
                    </div>
                    <!--//cwrap-->

                    <!--cwrap-->
                    <div class="cwrap">
                        <header class="s-title">
                            <h2 class="ribbon bright">Latest recipes</h2>
                        </header>

                        <!--entries latest recipes-->
                        <?php include_once './latest_recipes/latest_recipes.php'; ?>
                        <!--//entries latest recipes-->
                    </div>
                    <!--//cwrap-->

                    <!--cwrap-->
                    <div class="cwrap">
                        <header class="s-title">
                            <h2 class="ribbon bright">Latest Recipies Video</h2>
                        </header>
                        <!--entries-->
                        <div class="entries row">
                            <!--item-->
                            <?php include_once './latest_content_last_part/latest_content.php'; ?>
                        </div>
                        <!--//entries-->
                    </div>
                    <!--//cwrap-->
                </section>
                <!--//content-->


                <!--right sidebar-->
                <!--                <aside class="sidebar one-fourth">
                <?php //include_once './right_siderbar/right_sidebar.php'; ?>	
                                </aside>-->
            </div>
            <!--//right sidebar-->
        </div>
        <!--//wrap-->
    </main>
    <!--//main-->

    <!--call to action-->
    <section class="cta">
        <div class="wrap clearfix">
            <h2>Already convinced? This is a call to action section lorem ipsum dolor sit amet.</h2>
        </div>
    </section>
    <!--//call to action-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <script src="ajax/rating.js"></script>

    <script src="ajax/wishlist.js"></script>


    <!--//footer end-->
    <style type="text/css">
        .wishbar{
            line-height: 30px; font-size: 14px;
        }
        .wishresult{
            margin-top:9px;
        }
        .rating_result{
            float: right; margin-left:12px; margin-top: 5px; font-size: 14px;
        }

        .rating { 
            border: none;
            float: left;
        }

        .rating > input { display: none; } 
        .rating > label:before { 
            margin: 3px;
            font-size: 1.50em;
            font-family: FontAwesome;
            display: inline-block;
            content: "\f005";
        }

        .rating > .half:before { 
            content: "\f089";
            position: absolute;
        }

        .rating > label { 
            color: #ddd; 
            float: right; 
        }

        /***** CSS Magic to Highlight Stars on Hover *****/

        .rating > input:checked ~ label, /* show gold star when clicked */
        .rating:not(:checked) > label:hover, /* hover current star */
        .rating:not(:checked) > label:hover ~ label { color: #FFD700;  } /* hover previous stars in list */

        .rating > input:checked + label:hover, /* hover current star when changing rating */
        .rating > input:checked ~ label:hover,
        .rating > label:hover ~ input:checked ~ label, /* lighten current selection */
        .rating > input:checked ~ label:hover ~ label { color: #FFED85;  } 
    </style>



    <!--footer-->
    <?php include_once './segments/footer_part.php'; ?>
    <!--//footer end-->
    /*Bootstrap Carousel Touch Slider.




