-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 07, 2017 at 11:25 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cooking_recipes`
--

-- --------------------------------------------------------

--
-- Table structure for table `chef_detail`
--

CREATE TABLE `chef_detail` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `descriptions` text NOT NULL,
  `date` date NOT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chef_detail`
--

INSERT INTO `chef_detail` (`id`, `name`, `image`, `descriptions`, `date`, `is_active`) VALUES
(10, 'tom', 'HD-beauty-chef-pictures-43575.jpg', 'gjkkkkkkkkkkkkk', '2017-05-29', 1),
(11, 'kon', '1496038126o.jpg', 'yguyuy', '2017-05-29', 1),
(12, 'hamdam khan', '', 'cook of Indeian', '2017-05-31', 0),
(14, 'Munira', 'hi', 'dd', '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `chef_recipes`
--

CREATE TABLE `chef_recipes` (
  `id` int(11) NOT NULL,
  `chef_id` int(20) NOT NULL,
  `recipes_id` int(20) NOT NULL,
  `date` date NOT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chef_recipes`
--

INSERT INTO `chef_recipes` (`id`, `chef_id`, `recipes_id`, `date`, `is_active`) VALUES
(1, 4, 16, '2017-05-20', 0),
(2, 4, 18, '2017-05-20', 0);

-- --------------------------------------------------------

--
-- Table structure for table `contact_request`
--

CREATE TABLE `contact_request` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `descriptions` text NOT NULL,
  `date` date NOT NULL,
  `status` enum('Pending','Reject','In Progress','Complete') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_request`
--

INSERT INTO `contact_request` (`id`, `name`, `email`, `subject`, `descriptions`, `date`, `status`) VALUES
(1, 'sdadas', 'monira@gmail.com', '', 'sdsasadas dasd asd', '2017-05-29', 'Pending'),
(2, '', '', '', '', '2017-05-29', 'Pending'),
(3, '', '', '', '', '2017-05-29', 'Pending'),
(4, '', '', '', '', '2017-05-29', 'Pending'),
(5, '', '', '', '', '2017-05-29', 'Pending'),
(6, '', '', '', '', '2017-05-29', 'Pending'),
(7, '', '', '', '', '2017-05-29', 'Pending'),
(8, '', '', '', '', '2017-05-29', 'Pending'),
(9, '', '', '', '', '0000-00-00', 'Reject'),
(10, '', '', '', '', '0000-00-00', ''),
(11, 'ssas', 'munirAEIN@GAMIL.COM', '', 'sds', '2017-05-31', 'Pending'),
(12, '', '', '', '', '0000-00-00', 'Pending'),
(13, '', '', '', '', '0000-00-00', 'Complete'),
(14, 'rashed', 'lelin.rashed784@gmail.com', '', 'goiegoiejfoiejoifeo jfe0[fje', '2017-06-07', 'Pending');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_type` enum('user','admin') NOT NULL,
  `date` date NOT NULL,
  `status` enum('active','inactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `name`, `email`, `password`, `user_type`, `date`, `status`) VALUES
(1, 'Anu', 'anu@gmail.com', '123', 'user', '2017-05-24', 'active'),
(2, 'Fahad Bhuyian', 'f.bhuyian@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'user', '0000-00-00', 'active'),
(3, 'Fahad Bhuyian', 'f.bhuyians@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'user', '0000-00-00', 'active'),
(4, 'Fahad Bhuyian', 'f.bhuyans@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'user', '0000-00-00', 'active'),
(5, 'Fahad Bhuyian', 'f.bhuyns@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'user', '0000-00-00', 'active'),
(6, 'Fahad Bhuyian', 'f.bhuns@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'user', '0000-00-00', 'active'),
(7, 'munira', 'monira@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'user', '0000-00-00', 'active'),
(8, '', '', 'd41d8cd98f00b204e9800998ecf8427e', 'user', '0000-00-00', 'active'),
(9, 'munira', 'muniraein@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'user', '0000-00-00', 'active'),
(11, 'Munira', 'monira@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'admin', '2017-05-28', 'active'),
(12, 'rashed', 'lelin.rashed784@gmail.com', '262e8e1c7deb1239f641452e2c76cbf1', 'user', '0000-00-00', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `customer_recipes_request`
--

CREATE TABLE `customer_recipes_request` (
  `id` int(11) NOT NULL,
  `customer_id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `ingredients` varchar(255) NOT NULL,
  `directions` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `status` enum('Approve','Cancel','Pending') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_recipes_request`
--

INSERT INTO `customer_recipes_request` (`id`, `customer_id`, `name`, `image`, `ingredients`, `directions`, `date`, `status`) VALUES
(1, 1, 'bangul vorta', '', 'zzzzzzzzzzzz', 'zzzzzzzzzzzz', '2017-05-22', 'Approve'),
(2, 2, 'Test', '', '', '', '2017-05-26', 'Pending'),
(3, 2, 'Test', '', '', '', '2017-05-26', 'Pending'),
(4, 2, 'Test', '', '', '', '2017-05-26', 'Pending'),
(5, 2, 'Test', '', '', '', '2017-05-26', 'Pending'),
(6, 2, 'Test', '', '', '', '2017-05-26', 'Pending'),
(7, 2, 'Test', '', 'sdasd asdsa dsa dsa dsad as dsadas', 'dsadsadas dsadasd asdasdasdas das', '2017-05-26', 'Pending'),
(8, 2, 'Test', '', 'sdasd asdsa dsa dsa dsad as dsadas', 'dsadsadas dsadasd asdasdasdas das', '2017-05-26', 'Pending'),
(9, 2, 'Test', '', 'sdasd asdsa dsa dsa dsad as dsadas', 'dsadsadas dsadasd asdasdasdas das', '2017-05-26', 'Pending'),
(10, 2, 'Test', '', 'sdasd asdsa dsa dsa dsad as dsadas', 'dsadsadas dsadasd asdasdasdas das', '2017-05-26', 'Pending'),
(11, 2, 'sdfdsfsdfdsf', '', 'sdfdsfsdfsdf', 'sdfdsfdsfsdfsd', '2017-05-26', 'Pending'),
(12, 2, 'zxcxzcxz', '', 'xzcxzcxz', 'cxzcxzc', '2017-05-26', 'Pending'),
(13, 2, 'dsfdsfdsf', '', 'dsfdsfsdf', 'sdfdsf', '2017-05-26', 'Pending'),
(14, 2, 'cxvcxvcxvcx', '', 'xcvcxvxv', 'xcvcxvcxvc', '2017-05-26', 'Pending'),
(15, 2, 'sdfsdfafasfsfa', '', 'sdafdsaasdf sdf ', 'sdaf sdaf sdaf sdafs ', '2017-05-26', 'Pending'),
(16, 2, 'asdsadasdsad', '', 'asdsadsadsa', 'asdasdsadasdsa', '2017-05-26', 'Pending'),
(17, 2, 'sdfdsfsdfdsfsdfsdf', '', 'sdfdsfsdfdsf', 'sdfsdfsdfdsfsd', '2017-05-26', 'Pending'),
(18, 2, 'sdfdsfsdfdsfsdfsdf', '', 'sdfdsfsdfdsf', 'sdfsdfsdfdsfsd', '2017-05-26', 'Pending'),
(19, 2, 'sdsdsdsds', '', 'sdsddsds', 'sddsddsdds', '2017-05-26', 'Pending'),
(20, 2, 'asdsadsadsad', '', 'asdsadsadsad', 'asdasdsadsa', '2017-05-26', 'Pending'),
(21, 2, 'asdsadsadsad', '2015-07-09-01-08-01-508 - Copy.jpg', 'asdsadsadsad', 'asdasdsadsa', '2017-05-26', 'Pending'),
(22, 2, 'asdsadsad', '', 'sadsads', 'asdsadsadsad', '2017-05-26', 'Pending'),
(23, 2, 'sdfdsfdsfdsfdsf', '2015-07-09-01-08-01-508 - Copy.jpg', 'sdfsdfdsfds', 'dsfdsfdsfdsfds', '2017-05-26', 'Pending'),
(24, 2, 'Test', '2015-07-09-01-08-01-508 - Copy.jpg', 'df d d dfd d fd', 'd d d d d d d d d ', '2017-05-27', 'Approve'),
(25, 9, 'drinks', 'green-mango-sharbat1-716x1024.jpg', '\n    Green Mango/Kacha Aam - 3 medium boiled & mashed\n    Sugar - 1/2 cup\n    Ginger - 1 teaspoon grated\n    Fresh mint leaves - 1/2 cup\n    Bit lobon/Black salt - 1 teaspoon\n    Lemon Juice from - 1 medium\n    Ice cool water - 5 cup\n', '\n    Boil, peel and mash the green mango. Let them cool.\n    In a small vessel boil sugar, ginger and 2 cups of water until sugar is dissolve. Remove from flame and let them cool.\n    In a blender pour remaining water, mango, salt, mint leaves, and sugar ', '2017-05-28', 'Pending'),
(26, 0, 'dim', 'Forside-Logo.png', 'geegsb', 'hrrrggrgre', '2017-06-07', 'Pending'),
(27, 0, 'dim', 'Forside-Logo.png', 'geegsb', 'hrrrggrgre', '2017-06-07', 'Pending'),
(28, 0, 'dim', 'Forside-Logo.png', 'geegsb', 'hrrrggrgre', '2017-06-07', 'Pending'),
(29, 0, 'dim', 'Forside-Logo.png', 'geegsb', 'hrrrggrgre', '2017-06-07', 'Pending'),
(30, 0, 'dim', 'Forside-Logo.png', 'geegsb', 'hrrrggrgre', '2017-06-07', 'Pending'),
(31, 0, 'dim', 'Forside-Logo.png', 'geegsb', 'hrrrggrgre', '2017-06-07', 'Pending'),
(32, 0, 'dim', 'Forside-Logo.png', 'geegsb', 'hrrrggrgre', '2017-06-07', 'Pending'),
(33, 0, 'dim', 'Forside-Logo.png', 'geegsb', 'hrrrggrgre', '2017-06-07', 'Pending'),
(34, 12, 'geee', 'Forside-Logo.png', 'brgrrtf', 'ertrrtretrefer', '2017-06-07', 'Pending');

-- --------------------------------------------------------

--
-- Table structure for table `recipes`
--

CREATE TABLE `recipes` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(20) NOT NULL,
  `image` varchar(255) NOT NULL,
  `ingredients` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `directions` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `is_featured` tinyint(1) NOT NULL,
  `is_slider` tinyint(1) NOT NULL,
  `date` date NOT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recipes`
--

INSERT INTO `recipes` (`id`, `name`, `category_id`, `image`, `ingredients`, `directions`, `is_featured`, `is_slider`, `date`, `is_active`) VALUES
(15, 'brekfast', 0, '14524599_1794628680753688_7253635582383656145_o.jpg', 'dff', 'sf', 0, 0, '2017-05-20', 0),
(17, 'salad', 0, '12593564_1728583094024914_557666533694921425_o.jpg', 'kkkkkkkkkkkkkk', 'nnnnnnnnnnnnnnnnnnnnnnn', 1, 0, '2017-05-20', 1),
(18, 'poding', 2, 'Rajbhog-gobengal-1.jpg', 'zzzzzzzzzzz', 'zzzzzzzzzzzzzzzz', 0, 1, '2017-05-20', 0),
(20, 'cold coffee', 0, 'ice-cream-563808_960_720.jpg', 'Ingredients: (1 cup = 250 ml)\r\n\r\n    1.5 tablespoons instant coffee\r\n    ¼ cup water or 62.5 ml water\r\n    4 tablespoon sugar or as required\r\n    2.5 cups milk or 625 ml milk\r\n    1 or 2 vanilla ice cream scoops for the milkshake\r\n    3 to 6 vanilla ice c', 'How to make recipe\r\nmaking coffee solution:\r\n\r\n    in a small bowl, take 1.5 tbsp instant coffee.\r\n\r\n    add ¼ cup warm or slightly hot water. \r\n\r\n    mix very well with a spoon.\r\nmaking cold coffee:\r\n\r\n    now pour this mixture in a blender jar.\r\n    add', 1, 1, '2017-05-31', 1),
(21, 'testy salad', 0, 'salad.jpg', '1) Choose a direction for your salad.\r\n\r\nItalian? Mexican? Garden vegetable? Seasonal? Deciding on a “theme” will help to narrow down your ingredients.\r\n\r\n2) Grab your greens and a salad bowl.\r\n\r\nCrunchy light green lettuces like Romaine are great for hea', '3) Choose two to four seasonal fruits and vegetables.\r\n\r\nSalad is all about the produce, so use the best you can find, which means buying whatever is in season. Cut your ingredients into bite sized pieces and add them to your greens.\r\n\r\nTasty choices for ', 1, 1, '2017-05-31', 1);

-- --------------------------------------------------------

--
-- Table structure for table `recipes_category`
--

CREATE TABLE `recipes_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `is_top_nav` tinyint(1) NOT NULL,
  `date` date NOT NULL,
  `status` enum('active','inactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recipes_category`
--

INSERT INTO `recipes_category` (`id`, `name`, `is_top_nav`, `date`, `status`) VALUES
(2, 'salad', 1, '2017-05-18', 'active'),
(4, 'drinks', 1, '2017-05-20', 'active'),
(8, 'vorta', 1, '2017-05-21', 'active'),
(9, 'acher', 1, '2017-05-24', ''),
(14, 'Dessert', 1, '2017-05-25', 'active'),
(15, 'Tea & coffee', 1, '2017-06-01', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `recipes_rating`
--

CREATE TABLE `recipes_rating` (
  `id` int(11) NOT NULL,
  `recipes_id` int(20) NOT NULL,
  `rating` int(20) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recipes_rating`
--

INSERT INTO `recipes_rating` (`id`, `recipes_id`, `rating`, `date`) VALUES
(1, 14, 2, '2017-05-14'),
(2, 15, 5, '2017-05-14'),
(3, 18, 3, '2017-05-14'),
(4, 3, 5, '2017-05-14'),
(5, 17, 3, '2017-05-14'),
(6, 0, 0, '2017-05-26'),
(7, 15, 4, '2017-05-26'),
(8, 15, 4, '2017-05-26'),
(9, 16, 4, '2017-05-26'),
(10, 15, 4, '2017-05-26'),
(11, 16, 4, '2017-05-26'),
(12, 15, 4, '2017-05-26'),
(13, 15, 4, '2017-05-26'),
(14, 15, 4, '2017-05-26'),
(15, 15, 4, '2017-05-26'),
(16, 15, 4, '2017-05-26'),
(17, 15, 4, '2017-05-26'),
(18, 15, 4, '2017-05-26'),
(19, 15, 4, '2017-05-26'),
(20, 15, 4, '2017-05-26'),
(21, 16, 4, '2017-05-26'),
(22, 15, 4, '2017-05-26'),
(23, 15, 4, '2017-05-26'),
(24, 15, 4, '2017-05-26'),
(25, 15, 4, '2017-05-26'),
(26, 15, 4, '2017-05-26'),
(27, 16, 4, '2017-05-26'),
(28, 15, 4, '2017-05-26'),
(29, 16, 4, '2017-05-26'),
(30, 15, 4, '2017-05-26'),
(31, 16, 4, '2017-05-26'),
(32, 16, 5, '2017-05-26'),
(33, 16, 5, '2017-05-26'),
(34, 16, 5, '2017-05-26'),
(35, 16, 5, '2017-05-26'),
(36, 16, 5, '2017-05-26'),
(37, 16, 5, '2017-05-26'),
(38, 16, 5, '2017-05-26'),
(39, 16, 5, '2017-05-26'),
(40, 15, 4, '2017-05-26'),
(41, 21, 1, '2017-05-31'),
(42, 21, 2, '2017-05-31'),
(43, 21, 3, '2017-05-31'),
(44, 21, 4, '2017-05-31'),
(45, 20, 5, '2017-05-31'),
(46, 20, 4, '2017-05-31'),
(47, 20, 1, '2017-05-31'),
(48, 20, 2, '2017-05-31'),
(49, 20, 3, '2017-05-31'),
(50, 20, 4, '2017-05-31'),
(51, 21, 5, '2017-06-07'),
(52, 20, 5, '2017-06-07'),
(53, 21, 5, '2017-06-07'),
(54, 21, 5, '2017-06-07'),
(55, 21, 5, '2017-06-07'),
(56, 21, 5, '2017-06-07'),
(57, 20, 5, '2017-06-07'),
(58, 18, 5, '2017-06-07'),
(59, 18, 3, '2017-06-07'),
(60, 18, 3, '2017-06-07'),
(61, 18, 3, '2017-06-07'),
(62, 18, 4, '2017-06-07');

-- --------------------------------------------------------

--
-- Table structure for table `recipes_video`
--

CREATE TABLE `recipes_video` (
  `id` int(11) NOT NULL,
  `link` varchar(255) NOT NULL,
  `recipes_id` int(20) NOT NULL,
  `date` date NOT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recipes_video`
--

INSERT INTO `recipes_video` (`id`, `link`, `recipes_id`, `date`, `is_active`) VALUES
(1, 'https://www.youtube.com/embed/0Y90CXoIKh8', 17, '2017-05-22', 0),
(10, 'https://www.youtube.com/embed/TLH8fqmrHcA', 18, '2017-05-28', 1),
(11, 'https://www.youtube.com/embed/CMN9E0qaPFg', 16, '2017-05-29', 1),
(12, 'https://www.youtube.com/embed/jRzWiYRiIEw', 15, '2017-05-28', 1);

-- --------------------------------------------------------

--
-- Table structure for table `recipes_view`
--

CREATE TABLE `recipes_view` (
  `id` int(11) NOT NULL,
  `recipes_id` int(11) NOT NULL,
  `user_ip` varchar(255) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recipes_view`
--

INSERT INTO `recipes_view` (`id`, `recipes_id`, `user_ip`, `date`) VALUES
(1, 15, '11111', '2017-05-14'),
(2, 1, '11111', '2017-05-14'),
(3, 18, '11111', '2017-05-14'),
(4, 20, '11111', '2017-05-14'),
(5, 17, '11111', '2017-05-14'),
(6, 14, '11111', '2017-05-14'),
(7, 22, '11111', '2017-05-14'),
(8, 21, '11111', '2017-05-14'),
(9, 20, '::1', '2017-06-01'),
(10, 20, '::1', '2017-06-01'),
(11, 20, '::1', '2017-06-01'),
(12, 0, '::1', '2017-06-01'),
(13, 0, '::1', '2017-06-02'),
(14, 0, '::1', '2017-06-03'),
(15, 0, '::1', '2017-06-04'),
(16, 0, '::1', '2017-06-05'),
(17, 0, '::1', '2017-06-05'),
(18, 21, '::1', '2017-06-05'),
(19, 21, '::1', '2017-06-06'),
(20, 0, '::1', '2017-06-07'),
(21, 21, '::1', '2017-06-07'),
(22, 20, '::1', '2017-06-07'),
(23, 18, '::1', '2017-06-07'),
(24, 21, '::1', '2017-06-07'),
(25, 21, '::1', '2017-06-07'),
(26, 17, '::1', '2017-06-07'),
(27, 21, '::1', '2017-06-07'),
(28, 17, '::1', '2017-06-07'),
(29, 21, '::1', '2017-06-07'),
(30, 17, '::1', '2017-06-07'),
(31, 21, '::1', '2017-06-07');

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `id` int(11) NOT NULL,
  `recipes_id` int(20) NOT NULL,
  `customer_id` int(20) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wishlist`
--

INSERT INTO `wishlist` (`id`, `recipes_id`, `customer_id`, `date`) VALUES
(1, 14, 1, '2017-05-24'),
(2, 15, 0, '2017-05-26'),
(3, 15, 0, '2017-05-26'),
(4, 16, 0, '2017-05-26'),
(5, 17, 0, '2017-05-26'),
(6, 17, 0, '2017-05-26'),
(7, 17, 0, '2017-05-26'),
(8, 15, 0, '2017-05-26'),
(9, 15, 2, '2017-05-26'),
(10, 15, 2, '2017-05-26'),
(11, 16, 2, '2017-05-26'),
(12, 16, 2, '2017-05-26'),
(13, 15, 9, '2017-05-28'),
(14, 18, 9, '2017-05-28'),
(15, 17, 9, '2017-05-28');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chef_detail`
--
ALTER TABLE `chef_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chef_recipes`
--
ALTER TABLE `chef_recipes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_request`
--
ALTER TABLE `contact_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_recipes_request`
--
ALTER TABLE `customer_recipes_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recipes`
--
ALTER TABLE `recipes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recipes_category`
--
ALTER TABLE `recipes_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recipes_rating`
--
ALTER TABLE `recipes_rating`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recipes_video`
--
ALTER TABLE `recipes_video`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recipes_view`
--
ALTER TABLE `recipes_view`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chef_detail`
--
ALTER TABLE `chef_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `chef_recipes`
--
ALTER TABLE `chef_recipes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `contact_request`
--
ALTER TABLE `contact_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `customer_recipes_request`
--
ALTER TABLE `customer_recipes_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `recipes`
--
ALTER TABLE `recipes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `recipes_category`
--
ALTER TABLE `recipes_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `recipes_rating`
--
ALTER TABLE `recipes_rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `recipes_video`
--
ALTER TABLE `recipes_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `recipes_view`
--
ALTER TABLE `recipes_view`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
